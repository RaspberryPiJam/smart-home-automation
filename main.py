from Events.MotionSensorEvent import MotionSensorEvent
from Events.RemoteStepperMotorControllerEvent import RemoteStepperMotorControllerEvent
from Events.LightButtonEvent import LightButtonEvent
from Events.ButtonStepperMotorControllerEvent import ButtonStepperMotorControllerEvent
from Events.DomoticzEvent import HandlerProxy
from Events.RemoteLightContollerEvent import RemoteLightContollerEvent
from Observers.LedDriver import LedDriver
from Observers.StepMotorDriver import StepMotorDriver
from Utils.ConfigParser import ConfigParser
from Utils.DomoticzHandler import DomoticzHandler
from Utils.TemperatureHumiditySupervision import TemperatureHumiditySupervision
from Utils.LircSignalHandler import LircSignalHandler
from http.server import HTTPServer
import threading


def create_led_device():
    config = ConfigParser.get_instance()
    device = LedDriver(config.get_device_idx(LedDriver.get_name()))
    triggers = []
    policies_names = config.get_active_policy_for_device(LedDriver.get_name())
    for policy_name in policies_names:
        if policy_name == "MotionSensor":
            triggers.append(MotionSensorEvent())
        elif policy_name == "LightButton":
            triggers.append(LightButtonEvent())
        elif policy_name == "LightButtonIR":
            triggers.append(RemoteLightContollerEvent())
    for trigger in triggers:
        trigger.attach(device)
        trigger.start()
    return triggers, device


def create_step_motor_device():
    config = ConfigParser.get_instance()
    device = StepMotorDriver(config.get_device_idx(StepMotorDriver.get_name()))
    triggers = []
    policies_names = config.get_active_policy_for_device(StepMotorDriver.get_name())
    for policy_name in policies_names:
        if policy_name == "StepMotorControlButtons":
            triggers.append(ButtonStepperMotorControllerEvent())
        elif policy_name == "StepMotorButtonsIR":
            triggers.append(RemoteStepperMotorControllerEvent())

    for trigger in triggers:
        trigger.attach(device)
        trigger.start()
    return triggers, device


def run_domoticz(device_list, server_class=HTTPServer, handler_class=DomoticzHandler, port=80):
    for device in device_list:
        HandlerProxy.attach(device)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()


def run_temperature_supervision():
    temp_hum_sup = TemperatureHumiditySupervision()
    temp_hum_sup.start()
    return temp_hum_sup


def run_lirc_handler():
    lirc_handler = LircSignalHandler()
    lirc_handler.start()
    return lirc_handler


if __name__ == '__main__':
    threads = [create_led_device(), create_step_motor_device()]
    devices_list = []
    triggers_list = []
    for trigger, device in threads:
        devices_list.append(device)
        triggers_list.append(trigger)

    triggers_list.append(run_temperature_supervision())
    triggers_list.append(run_lirc_handler())
    run_domoticz(devices_list)

    for trigger in triggers_list:
        trigger.join()
