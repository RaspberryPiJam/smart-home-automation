from enum import Enum


class Direction(Enum):
    UP = 1
    STOP = 0
    DOWN = -1


def str2bool(word):
    return word.lower() in ("yes", "true", "t", "1")


def action2direction(word):
    if word.lower() == "up":
        return Direction.UP
    elif word.lower() == "down":
        return Direction.DOWN
    elif word.lower() == "stop":
        return Direction.STOP
