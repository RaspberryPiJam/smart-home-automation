from http.server import BaseHTTPRequestHandler
from Events.DomoticzEvent import HandlerProxy


class DomoticzHandler(BaseHTTPRequestHandler):

    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        request_path = self.path
        _, device, action = request_path.split("/")
        HandlerProxy.handle_event(device, action)

