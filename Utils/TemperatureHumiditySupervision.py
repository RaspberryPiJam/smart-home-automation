import threading
import time
import Adafruit_DHT
from Utils.ConfigParser import ConfigParser
from Utils.DomoticzNotifier import DomoticzNotifier


class TemperatureHumiditySupervision(threading.Thread):

    def __init__(self):
        super().__init__()
        self.__pin = ConfigParser.get_instance().get_temp_hum_sensor_pinout()
        self.__idx = ConfigParser.get_instance().get_device_idx(self.getName())
        self.__sensor_type = Adafruit_DHT.DHT11

    @staticmethod
    def getName():
        return "TempHumSensor"

    def run(self):
        while True:
            humidity, temperature = Adafruit_DHT.read_retry(self.__sensor_type, self.__pin)
            DomoticzNotifier.update_temperature_humidity(self.__idx, temperature, humidity)
            time.sleep(60)
