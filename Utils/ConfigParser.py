import xml.etree.ElementTree as ElementTree
from Utils.HelperTypes import action2direction


class ConfigParser:
    __instance = None
    __tree = None
    __root = None

    def __init__(self):
        if ConfigParser.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            ConfigParser.__instance = self
            self.__initialize_structure()

    @staticmethod
    def get_instance():
        if ConfigParser.__instance is None:
            ConfigParser()
        return ConfigParser.__instance

    def __initialize_structure(self):
        self.__tree = ElementTree.parse('config.xml')
        self.__root = self.__tree.getroot()

    def __get_device_by_name(self, name):
        devices = self.__root.find("devices")
        for child in devices:
            if child.get('name') == name:
                return child

    def __get_policy_by_name(self, name):
        devices = self.__root.find("policies")
        for child in devices:
            if child.get('name') == name:
                return child

    def get_device_idx(self, name):
        device = self.__get_device_by_name(name)
        return device.find('domoticzIdx').text

    def get_device_domoticz_representation(self, name):
        representation_list = {}
        device = self.__get_device_by_name(name)
        representation = device.find('domoticzValueRepresentation')
        for state in representation:
            representation_list[action2direction(state.get('name'))] = state.get('representation')
        return representation_list

    def get_active_policy_for_device(self, device_name):
        policies = []
        device = self.__get_device_by_name(device_name)
        triggers = device.find('triggers')
        for policy in triggers:
            if policy.get('enabled') == "true":
                policies.append(policy.get('name'))
        return policies

    def get_led_device_pinout(self):
        policy = self.__get_device_by_name('LedDevice')
        return int(policy.find('pinout').find('pin').text)

    def get_temp_hum_sensor_pinout(self):
        policy = self.__get_device_by_name('TempHumSensor')
        return int(policy.find('pinout').find('pin').text)

    def get_active_policy_for_led_device(self):
        return self.get_active_policy_for_device("StepMotorDevice")

    def get_stepper_motor_pinout(self):
        pins = []
        device = self.__get_device_by_name('StepMotorDevice')
        pinout = device.find('pinout')
        for pin in pinout:
            pins.append(int(pin.text))
        return pins

    def get_active_policy_for_stepper_motor(self):
        return self.get_active_policy_for_device("StepMotorDevice")

    def get_motion_sensor_pinout(self):
        policy = self.__get_policy_by_name('MotionSensor')
        return int(policy.find('pinout').find('pin').text)

    def get_light_button_pinout(self):
        policy = self.__get_policy_by_name('LightButton')
        return int(policy.find('pinout').find('pin').text)

    def get_stepper_motor_buttons_pinout(self):
        pinout_list = {}
        policy = self.__get_policy_by_name('StepMotorControlButtons')
        pinout = policy.find('pinout')
        for pin in pinout:
            pinout_list[action2direction(pin.get('action'))] = int(pin.text)
        return pinout_list

    def get_stepper_motor_active_keys(self):
        key_list = {}
        policy = self.__get_policy_by_name('StepMotorButtonsIR')
        action_keys = policy.find('actionKeys')
        for key in action_keys:
            key_list[key.get('action')] = key.get('name')
        return key_list

    def get_light_ir_active_key(self):
        key_list = {}
        policy = self.__get_policy_by_name('LightButtonIR')
        action_keys = policy.find('actionKeys')
        for key in action_keys:
            key_list[key.get('action')] = key.get('name')
        return key_list

