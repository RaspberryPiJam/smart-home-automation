import threading
import lirc


class LircSignalHandler(threading.Thread):
    __objects_list = []
    __current_signals = [(None, [])]
    __condition = threading.Condition()

    def __init__(self):
        super().__init__()
        lirc.init("myprogram")

    @staticmethod
    def __handle_lirc_signal():
        lirc_signal = lirc.nextcode()
        if lirc_signal:
            return lirc_signal[0]

    @staticmethod
    def subscribe(subscriber_name):
        __class__.__objects_list.append(subscriber_name)

    @staticmethod
    def __wait_for_signal():
        __class__.__condition.acquire()
        __class__.__condition.wait()
        __class__.__condition.release()

    @staticmethod
    def get_lirc_signal(subscriber_name):
        __class__.__clear_already_handled_signals()
        while True:
            for signal in __class__.__current_signals:
                signal_code, already_handled_by = signal
                if already_handled_by.count(subscriber_name) == 0:
                    already_handled_by.append(subscriber_name)
                    return signal_code
                else:
                    continue
            __class__.__wait_for_signal()

    def run(self):
        while True:
            lirc_code = self.__handle_lirc_signal()
            if lirc_code is not None:
                self.__current_signals.append((lirc_code, []))
                self.__condition.acquire()
                self.__condition.notify_all()
                self.__condition.release()

    @staticmethod
    def __clear_already_handled_signals():
        for signal in __class__.__current_signals:
            _, already_handled_by = signal
            if len(already_handled_by) == len(__class__.__objects_list):
                __class__.__current_signals.remove(signal)
