import urllib.request


class DomoticzNotifier:
    __domoticz_server = "localhost"
    __domoticz_port = 8080

    @staticmethod
    def update_device_state(device_idx, nvalue, svalue):
        if svalue is None:
            url_address = "http://{0}:{1}/json.htm?type=command&param=udevice&idx={2}&nvalue={3}".format(
                __class__.__domoticz_server, __class__.__domoticz_port, device_idx, nvalue)
            print(url_address)
            urllib.request.urlopen(url_address).read()
        else:
            url_address = "http://{0}:{1}/json.htm?type=command&param=udevice&idx={2}&nvalue={3}&svalue={4}".format(
                __class__.__domoticz_server, __class__.__domoticz_port, device_idx, nvalue, svalue)
            print(url_address)
            urllib.request.urlopen(url_address).read()

    @staticmethod
    def update_temperature_humidity(device_idx, temp, humidity):
        url = "http://{0}:{1}/json.htm?type=command&param=udevice&idx={2}&nvalue=0&svalue={3};{4};0".format(
            __class__.__domoticz_server, __class__.__domoticz_port, device_idx, temp, humidity)
        print(url)
        urllib.request.urlopen(url).read()

