from abc import abstractmethod
from Utils.DomoticzNotifier import DomoticzNotifier


class Observer:
    def __init__(self, idx):
        self._subject = None
        self._observer_state = None
        self._idx = idx

    @abstractmethod
    def driver_setup(self):
        pass

    @abstractmethod
    def update(self, arg):
        pass

    @abstractmethod
    def cast_device_state_to_domoticz_nvalue(self):
        return None

    @abstractmethod
    def cast_device_state_to_domoticz_svalue(self):
        return None

    @staticmethod
    @abstractmethod
    def get_name():
        pass

    def update_domoticz_device_state(self):
        DomoticzNotifier.update_device_state(self._idx, self.cast_device_state_to_domoticz_nvalue(),
                                             self.cast_device_state_to_domoticz_svalue())




