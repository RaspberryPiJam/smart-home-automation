import threading
from Observers.Observer import Observer
from Utils.HelperTypes import Direction
import RPi.GPIO as GPIO
import time
from Utils.ConfigParser import ConfigParser


class StepMotorDriver(Observer):
    stepper_thread = None

    def __init__(self, idx):
        super().__init__(idx)
        self.stepper_thread = StepperThread(self)
        self.stepper_thread.start()
        self.domoticz_value_representation = self.__get_value_representation()

    def driver_setup(self):
        pass

    def update(self, arg):
        self._observer_state = arg
        self.update_domoticz_device_state()

    @staticmethod
    def get_name():
        return "StepMotorDevice"

    def get_direction(self):
        return self._observer_state

    def cast_device_state_to_domoticz_nvalue(self):
        return 1

    def cast_device_state_to_domoticz_svalue(self):
        return self.domoticz_value_representation[self._observer_state]

    def __get_value_representation(self):
        return ConfigParser.get_instance().get_device_domoticz_representation(self.get_name())


class StepperThread(threading.Thread):
    __stepper_driver = None
    __seq = [[1, 0, 0, 0],
             [1, 1, 0, 0],
             [0, 1, 0, 0],
             [0, 1, 1, 0],
             [0, 0, 1, 0],
             [0, 0, 1, 1],
             [0, 0, 0, 1],
             [1, 0, 0, 1]]
    __control_pin = None

    def __init__(self, stepper_driver):
        self.__stepper_driver = stepper_driver
        self.__control_pin = ConfigParser.get_instance().get_stepper_motor_pinout()
        super().__init__()

    def run(self):
        GPIO.setmode(GPIO.BOARD)
        # TODO: Setup each pin connected to stepper motor to output

        while True:
            time.sleep(0.1)
            # TODO: Program the GPIO sequence for stepper motor movement.
            # TODO: __seq describes each step for 4 stepper motor's pins.
            # TODO: Direction of stepper movement can be accessed by StepMotorDriver
