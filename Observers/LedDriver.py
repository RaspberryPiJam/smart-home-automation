from Observers.Observer import Observer
import RPi.GPIO as GPIO
from Utils.ConfigParser import ConfigParser


class LedDriver(Observer):
    __pin = None

    def __init__(self, idx):
        super().__init__(idx)
        self._observer_state = False
        self.driver_setup()

    def driver_setup(self):
        self.__pin = int(ConfigParser.get_instance().get_led_device_pinout())
        GPIO.setmode(GPIO.BCM)
        # TODO: Setup pin

    def update(self, arg):
        self._observer_state = bool(arg)
        # TODO: set GPIO state depending on _observer_state
        self.update_domoticz_device_state()

    def cast_device_state_to_domoticz_nvalue(self):
        return int(self._observer_state)

    def cast_device_state_to_domoticz_svalue(self):
        return None

    @staticmethod
    def get_name():
        return "LedDevice"
