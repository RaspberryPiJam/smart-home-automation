from Events.Event import Event
import RPi.GPIO as GPIO
from Utils.ConfigParser import ConfigParser
import time


class LightButtonEvent(Event):
    __pin = None

    def __init__(self):
        super().__init__()
        self.subject_state = False
        self.event_setup()

    def event_setup(self):
        self.__pin = int(ConfigParser.get_instance().get_light_button_pinout())
        GPIO.setmode(GPIO.BCM)
        # TODO: Setup pin
        # TODO: you need to enable pull up resistor on each pin

    def run(self):
        while True:
            pass
            # TODO: Change subject_state in depend of button state
            time.sleep(0.1)
