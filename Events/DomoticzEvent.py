from Events.Event import Event
from Utils.HelperTypes import action2direction
from Utils.HelperTypes import str2bool
from Observers.LedDriver import LedDriver
from Observers.StepMotorDriver import StepMotorDriver


class DomoticzEvent(Event):

    def subject_state(self, arg):
        self._subject_state = arg

    def _notify_one(self, device, action):
        for observer in self._observers:
            if observer.get_name() == device:
                if device == LedDriver.get_name():
                    self.subject_state = (str2bool(action))
                    observer.update(self.subject_state)
                elif device == StepMotorDriver.get_name():
                    self.subject_state = action2direction(action)
                    observer.update(self.subject_state)


class HandlerProxy:
    handler_event = DomoticzEvent()

    @staticmethod
    def handle_event(device, action):
        __class__.handler_event._notify_one(device, action)

    @staticmethod
    def attach(observer):
        __class__.handler_event.attach(observer)
