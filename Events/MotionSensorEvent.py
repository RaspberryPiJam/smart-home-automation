from Events.Event import Event
import RPi.GPIO as GPIO
import time
from Utils.ConfigParser import ConfigParser


class MotionSensorEvent(Event):
    __pin = None

    def __init__(self):
        super().__init__()
        self.subject_state = False
        self.event_setup()

    def event_setup(self):
        self.__pin = int(ConfigParser.get_instance().get_motion_sensor_pinout())
        GPIO.setmode(GPIO.BCM)
        # TODO : Setup pin

    def run(self):
        # TODO: Implement reading of motion sensor state and change subject_state value
        # TODO: Good tip: Remember that sensor state can be changing more often then you supposed
        pass
