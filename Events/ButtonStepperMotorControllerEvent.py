from Events.Event import Event
import RPi.GPIO as GPIO
from Utils.ConfigParser import ConfigParser
from Utils.HelperTypes import Direction
import time


class ButtonStepperMotorControllerEvent(Event):
    __pinout = None

    def __init__(self):
        super().__init__()
        self.subject_state = Direction.STOP
        self.event_setup()

    def event_setup(self):
        self.__pinout = ConfigParser.get_instance().get_stepper_motor_buttons_pinout()
        GPIO.setmode(GPIO.BCM)
        # TODO: iterate over __pinout and setup pins.
        # TODO: function get_stepper_motor_buttons_pinout return dictionary dict(key: action, value: pin)
        # TODO: you need to enable pull up resistor on each pin

    def run(self):
        while True:
            for action, pin in self.__pinout.items():
                pass
                # TODO : Read pin state if pushed on assignee action to subject_state
            time.sleep(0.1)
