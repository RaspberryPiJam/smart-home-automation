from Events.Event import Event
from Utils.ConfigParser import ConfigParser
from Utils.LircSignalHandler import LircSignalHandler


class RemoteLightContollerEvent(Event):
    def __init__(self):
        super().__init__()
        self.__key = None
        self.subject_state = False
        self.event_setup()

    def event_setup(self):
        LircSignalHandler.subscribe(self.get_event_name())
        key_list = ConfigParser.get_instance().get_light_ir_active_key()
        self.__key = key_list['LightSwitch']

    @staticmethod
    def get_event_name():
        return "LightIrButtonEvent"

    def run(self):
        while True:
            lirc_signal = LircSignalHandler.get_lirc_signal(self.get_event_name())
            # TODO: set subject_state_in depending on received signal and expected signal (self.__key)
