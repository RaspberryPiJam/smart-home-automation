from Events.Event import Event
from Utils.HelperTypes import Direction
from Utils.ConfigParser import ConfigParser
from Utils.LircSignalHandler import LircSignalHandler


class RemoteStepperMotorControllerEvent(Event):
    __up = None
    __down = None
    __stop = None

    def __init__(self):
        super().__init__()
        self.subject_state = Direction.STOP
        self.event_setup()

    def run(self):
        while True:
            lirc_signal = LircSignalHandler.get_lirc_signal(self.get_event_name())
            # TODO: Check if remote_controller_code is not none,
            # TODO: than cast this value by using get_direction function and assignee it to subject_state

    def get_direction(self, remote_controller_code):
        if remote_controller_code == self.__up:
            return Direction.UP
        elif remote_controller_code == self.__down:
            return Direction.DOWN
        elif remote_controller_code == self.__stop:
            return Direction.STOP
        else:
            return None

    @staticmethod
    def get_event_name():
        return "RemoteStepperMotorControllerEvent"

    def event_setup(self):
        LircSignalHandler.subscribe(self.get_event_name())
        key_list = ConfigParser.get_instance().get_stepper_motor_active_keys()
        self.__up = key_list['up']
        self.__down = key_list['down']
        self.__stop = key_list['stop']
