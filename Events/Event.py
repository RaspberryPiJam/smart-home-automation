import threading


class Event(threading.Thread):

    def __init__(self):
        super().__init__()
        self._observers = set()
        self._subject_state = None

    def run(self):
        pass

    def event_setup(self):
        pass

    def attach(self, observer):
        observer._subject = self
        self._observers.add(observer)

    def detach(self, observer):
        observer._subject = None
        self._observers.discard(observer)

    def _notify(self):
        for observer in self._observers:
            observer.update(self._subject_state)

    @property
    def subject_state(self):
        return self._subject_state

    @subject_state.setter
    def subject_state(self, arg):
        self._subject_state = arg
        self._notify()
